import React from "react";

export default class FetchRandomUser extends React.Component {
  state = {
    loading: true,
  };

  async componentDidMount() {
    const url = "https://cworkdjangoinventory.herokuapp.com/api/inventory/?format=json";
    const response = await fetch(url);
    const data = await response.json();
    this.setState({ product: data.results[0], loading: false });
  }

  render() {
    if (this.state.loading) {
      return <div>loading...</div>;
    }

    if (!this.state.product) {
      return <div>didn't get a product</div>;
    }

    return (
      <div>
        <div>{this.state.product.title}</div>
        <div>{this.state.product.detail}</div>
        <div>{this.state.product.category}</div>
      </div>
    );
  }
}